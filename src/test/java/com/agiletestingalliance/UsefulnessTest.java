package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class UsefulnessTest {
    @Test
    public void UsefulnessupdateDesc() throws Exception {

        String myStr= new Usefulness().desc();
        assertTrue("Result", myStr.contains("DevOps is about transformation"));
        
    }

    @Test
    public void UsefulnessfunctionWF() throws Exception {

        int myVal= new Usefulness().functionWF();
        assertEquals("Result", 5, myVal);   
        
    }
}

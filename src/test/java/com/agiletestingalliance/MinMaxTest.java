package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void MinMaxFirstCall() throws Exception {

        int myVal= new MinMax().firstCall(5, 3);
         assertEquals("Result", 5, myVal);   
    }

     @Test
    public void MinMaxFirstCallWithBig() throws Exception {

        int myVal= new MinMax().firstCall(1, 2);
         assertEquals("Result", 2, myVal);   
    }

	 @Test
    public void MinMaxBar() throws Exception {

        String myStr= new MinMax().bar("Hello");
         assertEquals("Result", "Hello", myStr);   
    }
}
